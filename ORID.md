### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today we integrated the frontend and backend TodoList cases, developed the backend to implement the integration by way of TDD, and at the same time understood cross-domain related issues and used the backend to solve cross-domain issues. We also had an Ant design beauty pageant.In the integration case exercise, Replace the mock API with a local backend interface.In the afternoon, we used concept map to summarize the week and then had a Presentation. Today was a very profound day for our team, due to various reasons, the team was uncoordinated and there was a great lack of communication, which led to the failure of the Presentation, which greatly undermined our confidence and also revealed our internal problems.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Distressed.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	Team activities can show our ability and problems of internal collaboration. Through this failure, we deeply realized the importance of communication and reflected deeply. Lack of communication is a very significant problem in a team, and we also regard this time as a warning that no matter what, we should never take team communication lightly!

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

The idea of teamwork and communication is deeply embedded into future work, and is deeply remembered through this failure.
