package com.oocl.gtd.controller;

import com.oocl.gtd.service.TodoService;
import com.oocl.gtd.dto.TodoRequest;
import com.oocl.gtd.dto.TodoResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getTodos(){
        return todoService.list();
    }

    @PostMapping
    public TodoResponse save(@RequestBody TodoRequest todoRequest){
        return todoService.save(todoRequest);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") int id){
        todoService.delete(id);
    }

    @PutMapping("{id}")
    public void update(@PathVariable("id") int id, @RequestBody TodoRequest todoRequest){
        todoRequest.setId(id);
        todoService.update(todoRequest);
    }

    @GetMapping("{id}")
    public TodoResponse getTodoById(@PathVariable("id") int id){
        return todoService.getTodoById(id);
    }
}
