package com.oocl.gtd.mapper;

import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.dto.TodoRequest;
import com.oocl.gtd.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static Todo toTodo(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest, todo);
        return todo;
    }
}
