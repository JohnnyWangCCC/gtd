package com.oocl.gtd.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Builder
public class Todo {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;
    private String text;
    private Boolean done;

    public Todo() {
    }

    public Todo(Integer id, String text, Boolean done) {
        this.id = id;
        this.text = text;
        this.done = done;
    }
}
