package com.oocl.gtd.dto;

import lombok.Data;

@Data
public class TodoResponse {
    private Integer id;
    private String text;
    private Boolean done;
}
