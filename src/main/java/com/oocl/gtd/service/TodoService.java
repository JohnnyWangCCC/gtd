package com.oocl.gtd.service;

import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.exception.TodoNotFoundException;
import com.oocl.gtd.mapper.TodoMapper;
import com.oocl.gtd.repository.TodoRepository;
import com.oocl.gtd.dto.TodoRequest;
import com.oocl.gtd.dto.TodoResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> list() {
        return todoRepository.findAll().stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }

    public TodoResponse save(TodoRequest todoRequest) {
        return TodoMapper.toResponse(todoRepository.save(TodoMapper.toTodo(todoRequest)));
    }

    public void delete(int id) {
        todoRepository.deleteById(id);
    }

    public void update(TodoRequest todoRequest) {
        Optional<Todo> todoData = todoRepository.findById(todoRequest.getId());
        todoData.ifPresent(todo -> {
            if (todoRequest.getDone() != null) {
                todo.setDone(todoRequest.getDone());
            }
            if (todoRequest.getText() != null) {
                todo.setText(todoRequest.getText());
            }
            todoRepository.save(todo);
        });
    }

    public TodoResponse getTodoById(int id) {
        return TodoMapper.toResponse(todoRepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }
}
