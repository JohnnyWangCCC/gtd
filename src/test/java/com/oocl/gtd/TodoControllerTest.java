package com.oocl.gtd;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.repository.TodoRepository;
import com.oocl.gtd.dto.TodoRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Resource
    private MockMvc client;
    @Resource
    private TodoRepository todoRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todos_perform_get_given_todos_in_db() throws Exception {
        //given
        Todo readBook = Todo.builder().text("read 100 books").done(false).build();
        Todo doSomeSport = Todo.builder().text("do some sport").done(false).build();

        todoRepository.saveAll(List.of(readBook, doSomeSport));
        //when then
        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(readBook.getText()))
                .andExpect(jsonPath("$[0].done").value(readBook.getDone()))
                .andExpect(jsonPath("$[1].text").value(doSomeSport.getText()))
                .andExpect(jsonPath("$[1].done").value(doSomeSport.getDone()));
    }

    @Test
    void should_return_todos_perform_post_given_todos_in_db() throws Exception {
        //given
        TodoRequest readBook = TodoRequest.builder().text("read 100 books").done(false).build();
        //when then
        client.perform(MockMvcRequestBuilders.post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(readBook)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(readBook.getText()))
                .andExpect(jsonPath("$.done").value(readBook.getDone()));
    }

    @Test
    void should_return_void_perform_delete_given_todos_in_db() throws Exception {
        //given
        Todo readBook = Todo.builder().text("read 100 books").done(false).build();

        Todo todo = todoRepository.save(readBook);

        //when then
        client.perform(MockMvcRequestBuilders.delete("/todos/{id}", todo.getId()))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_void_perform_put_given_todo_with_update_done_in_db() throws Exception {
        //given
        Todo readBook = Todo.builder().text("read 100 books").done(false).build();

        Todo todo = todoRepository.save(readBook);
        todo.setDone(true);
        //when then
        client.perform(MockMvcRequestBuilders.put("/todos/{id}", todo.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(todo)))
                .andExpect(status().isOk());

        Todo todoGetById = todoRepository.findById(todo.getId()).get();
        Assertions.assertNotNull(todoGetById);
        Assertions.assertEquals(todo.getDone(), todoGetById.getDone());
    }

    @Test
    void should_return_void_perform_put_given_todo_with_update_text_in_db() throws Exception {
        //given
        Todo readBook = Todo.builder().text("read 100 books").done(false).build();

        Todo todo = todoRepository.save(readBook);
        todo.setText("read 100 books");
        //when then
        client.perform(MockMvcRequestBuilders.put("/todos/{id}", todo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todo)))
                .andExpect(status().isOk());

        Todo todoGetById = todoRepository.findById(todo.getId()).get();
        Assertions.assertNotNull(todoGetById);
        Assertions.assertEquals(todo.getText(), todoGetById.getText());
    }

    @Test
    void should_return_todo_perform_get_given_todo_id() throws Exception {
        //given
        Todo readBook = Todo.builder().text("read 100 books").done(false).build();

        Todo todo = todoRepository.save(readBook);

        //when then
        client.perform(MockMvcRequestBuilders.get("/todos/{id}", todo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(readBook.getId()))
                .andExpect(jsonPath("$.text").value(readBook.getText()))
                .andExpect(jsonPath("$.done").value(readBook.getDone()));
    }

    @Test
    void should_throw_todo_not_found_exception_perform_get_given_todo_id() throws Exception {
        //given
        Todo readBook = Todo.builder().id(1).text("read 100 books").done(false).build();
        //when then
        client.perform(MockMvcRequestBuilders.get("/todos/{id}", readBook.getId()))
                .andExpect(status().isNotFound());
    }
}
